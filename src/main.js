var colors = {
  light: [13, 13, 13],
  dark: [0, 0, 0],
  grid: [255, 255, 255],
  walker: [255, 255, 255]
}

var boardSize
var size = 7
var array = create2DArray(size, size, 0, true)
var popMin = 19
var popMax = 19
var walkers
var walkerArray = create2DArray(size, size, 0, true)
var pickedWalker
var neighbours
var pickedNeighbour

var frames = 0
var animation = {
  walker: false
}

function setup() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
  walkers = populateGrid(array)
  for (var i = 0; i < walkers.length; i++) {
    walkerArray[walkers[i][0]][walkers[i][1]] = 1
  }
}

function draw() {
  createCanvas(windowWidth, windowHeight)
  background(colors.light)
  rectMode(CENTER)
  colorMode(RGB, 255, 255, 255, 1)
  frameRate(60)

  fill(colors.dark)
  noStroke()
  rect(windowWidth * 0.5, windowHeight * 0.5, boardSize, boardSize)

  for (var i = 0; i < size; i++) {
    fill(colors.grid)
    rect(windowWidth * 0.5 + (i - Math.floor(size * 0.5)) * (42 / 768) * boardSize * (17 / size), windowHeight * 0.5, boardSize * 0.005 * (8 / size), (44 / 768) * boardSize * (17 / size) * (size - 0.5))
    rect(windowWidth * 0.5, windowHeight * 0.5 + (i - Math.floor(size * 0.5)) * (42 / 768) * boardSize * (17 / size), (44 / 768) * boardSize * (17 / size) * (size - 0.5), boardSize * 0.005 * (8 / size))
  }
  for (var i = 0; i < walkers.length; i++) {
    fill(colors.dark)
    stroke(colors.walker)
    strokeWeight(boardSize * 0.005 * (8 / size))
    push()
    translate(windowWidth * 0.5 + (walkers[i][0] - Math.floor(size * 0.5)) * (42 / 768) * boardSize * (17 / size), windowHeight * 0.5 + (walkers[i][1] - Math.floor(size * 0.5)) * (42 / 768) * boardSize * (17 / size))
    ellipse(0, 0, (44 / 768) * boardSize * (17 / size) * 0.5 * walkers[i][2], (44 / 768) * boardSize * (17 / size) * 0.5 * walkers[i][2])
    pop()
  }

  frames += deltaTime / 600
  if (frames > 2) {
    frames = 0
    if (animation.walker === false) {
      pickedWalker = pickRandomWalker(walkers)
      neighbours = getNeighbours(pickedWalker, walkerArray)
      if (neighbours.length !== 0) {
        pickedNeighbour = pickNeighbour(neighbours)
        if (pickedNeighbour.length !== 0) {
          animation.walker = true
        }
      }
    }
  }
  if (animation.walker === true) {
    if (pickedWalker.length !== 0) {
      moveWalker(pickedWalker, pickedNeighbour[0], pickedNeighbour[1])
    }
    setTimeout(function() {
      animation.walker = false
    }, 420)
  } else {
    if (pickedWalker !== undefined) {
      pickedWalker[0] = round(pickedWalker[0])
      pickedWalker[1] = round(pickedWalker[1])
      walkerArray = create2DArray(size, size, 0, true)
      for (var i = 0; i < walkers.length; i++) {
        walkerArray[round(walkers[i][0])][round(walkers[i][1])] = 1
      }
    }
  }

  if (pickedWalker !== undefined) {
    fill(colors.walker)
    stroke(colors.walker)
    strokeWeight(boardSize * 0.005 * (8 / size))
    push()
    translate(windowWidth * 0.5 + (pickedWalker[0] - Math.floor(size * 0.5)) * (42 / 768) * boardSize * (17 / size), windowHeight * 0.5 + (pickedWalker[1] - Math.floor(size * 0.5)) * (42 / 768) * boardSize * (17 / size))
    ellipse(0, 0, (44 / 768) * boardSize * (17 / size) * 0.5 * pickedWalker[2], (44 / 768) * boardSize * (17 / size) * 0.5 * pickedWalker[2])
    pop()
  }
}

function mousePressed() {
  array = create2DArray(size, size, 0, true)
  popMin = 1 + Math.floor(Math.random() * 20)
  popMax = 20 + Math.floor(Math.random() * 20)
  walkers = populateGrid(array)
  walkerArray = create2DArray(size, size, 0, true)
  for (var i = 0; i < walkers.length; i++) {
    walkerArray[walkers[i][0]][walkers[i][1]] = 1
  }
  pickedWalker = undefined
  neighbours = undefined
  pickedNeighbour = undefined
  animation.walker = false
}

function windowResized() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function moveWalker(walker, posX, posY) {
  var moveX = walker[0] - posX
  var moveY = walker[1] - posY
  walker[0] -= moveX * frames * 0.5
  walker[1] -= moveY * frames * 0.5
  return walker
}

function pickNeighbour(neighbours) {
  var picked = neighbours[Math.floor(Math.random() * neighbours.length)]
  return picked
}

function getNeighbours(walker, array) {
  var neighbours = []
  if (walker[0] > 0) {
    if (array[walker[0] - 1][walker[1]] === 0) {
      neighbours.push([walker[0] - 1, walker[1]])
    }
  }
  if (walker[0] < size - 1) {
    if (array[walker[0] + 1][walker[1]] === 0) {
      neighbours.push([walker[0] + 1, walker[1]])
    }
  }
  if (walker[1] > 0) {
    if (array[walker[0]][walker[1] - 1] === 0) {
      neighbours.push([walker[0], walker[1] - 1])
    }
  }
  if (walker[1] < size - 1) {
    if (array[walker[0]][walker[1] + 1] === 0) {
      neighbours.push([walker[0], walker[1] + 1])
    }
  }
  return neighbours
}

function pickRandomWalker(walkers) {
  var picked = walkers[Math.floor(Math.random() * walkers.length)]
  return picked
}

function populateGrid(array) {
  var population = popMin + Math.floor(Math.random() * (popMax - popMin))
  var possiblePos = []
  var walkers = []
  for (var i = 0; i < size; i++) {
    for (var j = 0; j < size; j++) {
      possiblePos.push([i, j])
    }
  }
  possiblePos = shuffleArray(possiblePos)
  for (var i = 0; i < population; i++) {
    walkers.push([possiblePos[possiblePos.length - 1][0], possiblePos[possiblePos.length - 1][1], Math.floor(Math.random() * 2 + 1) * 0.5])
    possiblePos.splice(-1, 1)
  }
  return walkers
}

function shuffleArray(array) {
  var currentIndex = array.length,
    temporaryValue, randomIndex
  while (0 !== currentIndex) {
    randomIndex = Math.floor(Math.random() * currentIndex)
    currentIndex -= 1
    temporaryValue = array[currentIndex]
    array[currentIndex] = array[randomIndex]
    array[randomIndex] = temporaryValue
  }
  return array
}

function create2DArray(numRows, numCols, init, bool) {
  var array = [];
  for (var i = 0; i < numRows; i++) {
    var columns = []
    for (var j = 0; j < numCols; j++) {
      if (bool === true) {
        columns[j] = init
      } else {
        columns[j] = j * numRows + i
      }
    }
    array[i] = columns
  }
  return array
}
